import { Component, OnInit } from '@angular/core';
import { TasksService } from '../tasks.service';

@Component({
  selector: 'app-task-form',
  templateUrl: './task-form.component.html',
  styleUrls: ['./task-form.component.css']
})
export class TaskFormComponent implements OnInit {

  constructor(private _tasks: TasksService) { }

  newTask:{
    name: string,
    date: Date,
    assigned: string
  };

  ngOnInit() {
    this.newTask = {
      name: '',
      date: new Date(),
      assigned: ''
    }
  }
  submit(taskform:any){
    if((this.newTask.name=='')||(this.newTask.assigned=='')){
      alert("please fill form");
    }else{
      console.log(this.newTask);
      this._tasks.tasks.push(this.newTask);
      this.newTask={
        name: '',
        date: new Date(),
        assigned: ''
      }
    }
     
  }
  
}

